Setup instructions:

```
npm install -g pm2
cd ~
mkdir node-red-1
cd node-red-1
git clone https://bitbucket.org/knagaitsev/node-red-template.git .
sudo npm install
chmod +x start.sh
./start.sh
```

to check that it is working:
```
sudo pm2 list
```

to make it restart on device restart:
```
sudo pm2 save
sudo pm2 startup
```
